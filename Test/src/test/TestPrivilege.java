package test;

import classes.Privilege;
import java.util.Scanner;

public class TestPrivilege {
	public void testPConstructor(){
		System.out.print( "Constructor\n\n" );
		Scanner in = new Scanner(System.in);
		System.out.print( "Input name:" );
		String name = in.next();
		System.out.print( "Input coefficient:" );

		float coefficient   = Float.parseFloat( in.next() );
		Privilege P = new Privilege( name, coefficient );
		P.getDump();
	}

	public void testPgets(){
		System.out.print( "Get-s\n\n" );
		String string = "123";
		float price   = ( float ) 123.4;

		Privilege P = new Privilege( string, price );

		System.out.println( "getName()" + P.getName() );
		System.out.println( "getCoefficient()" + P.getCoefficient() );

		P.getDump();
	}

	public void testTSsets(){
		System.out.print( "Set-s\n\n" );

		String string = "123";
		float price   = ( float ) 123.4;

		System.out.print( "Befor\n" );
		Privilege P = new Privilege( string, price );
		P.getDump();

		System.out.print( "After\n" );
		P.setName( "bus" );
		P.setCoefficient( 500 );
		P.getDump();
	}
	public void testTSequals(){
		System.out.print( "Equals\n\n" );
		Privilege P1 = new Privilege( "P1", 500 );
		Privilege P2 = new Privilege( "P1", 500 );
		Privilege P3 = new Privilege( "P2", 300 );
		Privilege P4 = new Privilege( "P1", 300 );
		Privilege P5 = new Privilege( "P2", 500 );
		System.out.print( "1: " ); P1.getDump();
		System.out.print( "2: " ); P2.getDump();
		System.out.print( "3: " ); P3.getDump();
		System.out.print( "4: " ); P4.getDump();
		System.out.print( "5: " ); P5.getDump();
		System.out.println( "TS1 == TS1 " + P1.equals( P1 ) );
		System.out.println( "TS1 == TS2 " + P2.equals( P2 ) );
		System.out.println( "TS1 == TS3 " + P1.equals( P3 ) );
		System.out.println( "TS1 == TS4 " + P1.equals( P4 ) );
		System.out.println( "TS1 == TS5 " + P1.equals( P5 ) );
	}
}
