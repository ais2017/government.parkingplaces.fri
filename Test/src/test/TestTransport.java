package test;

import classes.Transport;
import java.io.IOException;
import java.util.Scanner;

public class TestTransport {

	public void testTSconstructor() throws IOException {
		System.out.print( "Constructor\n\n" );
		Scanner in = new Scanner(System.in);
		System.out.print( "Input name:" );
		String name = in.next();
		System.out.print( "Input price:" );
		float price   = Float.parseFloat( in.next() );

		Transport TS = new Transport( name, price );
		TS.getDump();
	}

	public void testTSgets(){
		System.out.print( "Get-s\n\n" );
		String string = "bike";
		float price   = ( float ) 123.4;

		Transport TS = new Transport( string, price );

		System.out.println( "getName()" + TS.getName() );
		System.out.println( "getPrice()" + TS.getPrice() );

		TS.getDump();
	}

	public void testTSsets(){
		System.out.print( "Set-s\n\n" );

		String string = "bike";
		float price   = ( float ) 123.4;

		System.out.print( "Befor\n" );
		Transport TS = new Transport( string, price );
		TS.getDump();

		System.out.print( "After\n" );
		TS.setName( "bus" );
		TS.setPrice( 500 );
		TS.getDump();
	}
	public void testTSequals(){
		System.out.print( "Equals\n\n" );
		Transport TS1 = new Transport( "TS1", 500 );
		Transport TS2 = new Transport( "TS1", 500 );
		Transport TS3 = new Transport( "TS2", 300 );
		Transport TS4 = new Transport( "TS1", 300 );
		Transport TS5 = new Transport( "TS2", 500 );
		System.out.print( "TS1" ); TS1.getDump();
		System.out.print( "TS2" ); TS2.getDump();
		System.out.print( "TS3" ); TS3.getDump();
		System.out.print( "TS4" ); TS4.getDump();
		System.out.print( "TS5" ); TS5.getDump();
		System.out.println( "TS1 == TS1 " + TS1.equals( TS1 ) );
		System.out.println( "TS1 == TS2 " + TS1.equals( TS2 ) );
		System.out.println( "TS1 == TS3 " + TS1.equals( TS3 ) );
		System.out.println( "TS1 == TS4 " + TS1.equals( TS4 ) );
		System.out.println( "TS1 == TS5 " + TS1.equals( TS5 ) );
	}
}
