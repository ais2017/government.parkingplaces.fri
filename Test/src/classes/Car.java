package classes;

import java.util.ArrayList;
import java.util.Objects;

public class Car extends Transport {

    private String               number;
    private ArrayList<Privilege> listOfPrivilege;

    //constructor
    public Car( String name, float price, String number, ArrayList listOfPrivilege ) {
        super(name, price);
        this.number = number;
        this.listOfPrivilege = listOfPrivilege;
    }

    //get-s
    public String getNumber() {
        return number;
    }
	public ArrayList getListOfPrivilege() {
		return listOfPrivilege;
	}

	//set-s
    public void setNumber( String number ) {
        this.number = number;
    }
    public void setListOfPrivilege( ArrayList listOfPrivilege ) {
        this.listOfPrivilege = listOfPrivilege;
    }

	private int     findPrivilege( Privilege privilege ){
		for( int i = 0; i < listOfPrivilege.size(); ++i )
			if( listOfPrivilege.get( i ).equals( privilege ) )
				return i;
		return -1;
	}
	public  boolean addPrivilege( Privilege privilege ){
		int n = findPrivilege( privilege );
		if( n == -1 ){
			listOfPrivilege.add( privilege );
			return true;
		}
		return false;
	}
	public  boolean delPrivilege( Privilege privilege ){
		int n = findPrivilege( privilege );
		if( n != -1 )
		{
			listOfPrivilege.remove( n );
			return true;
		}
		return false;
	}

    public float calcCoef(){
        float value = 0;

        for( int i = 0; i < listOfPrivilege.size(); ++i )
            value  += listOfPrivilege.get( i ).getCoefficient();

		return value;
    }

    public boolean equals( Car car ) {
        return number.equals( car.getNumber() );
    }

    //DataBase
    public Parking getCar( String number ){
        return null;
    }
    public boolean writeCar( Parking parking )
    {
        return true;
    }

    public void getDump(){
        System.out.println( "Type of transport\t" + name + "\nPrice\t" + price );
        System.out.println( "\nNumber of car" + number);
        for( int i = 0; i < listOfPrivilege.size(); i++ ){
            System.out.println( "\nPrivilege №" + ( i + 1 ) );
            listOfPrivilege.get( i ).getDump();
        }
    }
}

