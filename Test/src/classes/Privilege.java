package classes;

public class Privilege {
    private String name;
    private float coefficient;

    //constructor
    public Privilege( String name, float coefficient ) {
        this.name = name;
        this.coefficient = coefficient;
    }

    //get-s
    public String getName() {
        return name;
    }
    public float  getCoefficient() {
        return coefficient;
    }

    //set-s
    public byte     setName( String newName ) {
        name = newName;
        return 0;
    }
    public boolean  setCoefficient( float newCoefficient ) {
       if( newCoefficient < 1 ) {
          coefficient = newCoefficient;
          return true;
       }
       else
        return false;
    }

    public boolean equals( Privilege privilege ){
        return name.equals( privilege.getName() ) & coefficient == privilege.getCoefficient();
    }

    public void getDump() {
        System.out.println( "Name:\t" + name + "\tCoefficient:\t" + coefficient );
    }
}
