package classes;

import java.util.ArrayList;

public class Place {
    private boolean              flag;
    private ArrayList<Transport> listOfTransport;
    private Car                  carOnPlace;
    private int                  beginTime;

    //constructor
    public Place( ArrayList listOfTransport, Car carOnPlace, int beginTime ) {
        this.flag            = true;
        this.listOfTransport = listOfTransport;
        this.carOnPlace      = carOnPlace ;
        this.beginTime       = beginTime;
    }

    //get-s
    public boolean   isFlag() {
        return flag;
    }
    public ArrayList getListOfTransport() {
        return listOfTransport;
    }
    public Car       getCarOnPlace() {
        return carOnPlace;
    }
    public int       getBeginTime() {
        return beginTime;
    }

    //set-s
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    public void setListOfTransport(ArrayList listOfTransport) {
        this.listOfTransport = listOfTransport;
    }
    public void setCarOnPlace( Car carOnPlace ) {
        this.carOnPlace = carOnPlace;
    }
    public void setBeginTime( int beginTime ) {
        this.beginTime = beginTime;
    }

    public boolean isEmpty()
    {
        return flag;
    }
    public boolean isEmpty( Car car ){
        for( int i = 0; i < listOfTransport.size(); ++ i )
            if( car.getName().equals( listOfTransport.get( i ).getName() ) & isEmpty() )
                return true;
        return false;
    }
    public int     calcTime( int time )
    {
        return  time - beginTime;
    }
    public void    exemptPlace(){
        flag        = false;
        carOnPlace  = null;
        beginTime   = -3808;
    }
    public boolean takePlace( Car newCar, int beginTime ){
        if( isEmpty( newCar ) ){
            carOnPlace = newCar;
            this.beginTime = beginTime;
            return true;
        }
        else
            return false;
    }

    private int     findTS( Transport TS){
        for( int i = 0; i < listOfTransport.size(); i++ ){
            if( listOfTransport.get( i ).getName().equals( TS.getName() ))
                return i;
        }
        return -1;
    }
    public  boolean addTS( Transport newTS ){
        if( findTS( newTS ) >= 0 )
        {
            listOfTransport.add( newTS );
            return true;
        }
        return false;
    }
    public  boolean deleteTS( Transport delTS ){
        int numb = findTS( delTS );
        if( numb >= 0 )
        {
            listOfTransport.remove( numb );
            return true;
        }
        return false;
    }

    public boolean equals( Place place ) {
        if( flag == place.isFlag() & listOfTransport.size() == place.getListOfTransport().size() )
            for( int i = 0; i < listOfTransport.size(); ++i )
                if( !listOfTransport.get( i ).equals( place.getListOfTransport().get( i ) ) )
                    return false;
        return true;
    }

    public void getDump(){
        if( isEmpty() )
            System.out.println( "Place is free");
        else
            System.out.println( "Place is taken");

        System.out.println( "Possible TS" );
        for( int i = 0; i < listOfTransport.size(); i++ ) {
            System.out.println( "№" + ( i + 1 ) );
            listOfTransport.get(i).getDump();
        }

        System.out.println( "Car" );
        carOnPlace.getDump();
    }
}
