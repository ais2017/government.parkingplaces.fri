package classes;

import java.util.ArrayList;
import java.util.SimpleTimeZone;

public class Parking {
    private String               adress;
	private int[]                workTime;
    private int                  numbOfPlace;
    private ArrayList<Place>     listOfPlaces;
    private ArrayList<Privilege> listOfPrivilege;

    //constructor
    public Parking( String adress, int numbOfPlace, ArrayList listOfPlaces, int[] workTime, ArrayList listOfPrivilege ) {
        this.adress       = adress;
        this.numbOfPlace  = numbOfPlace;
        this.listOfPlaces = listOfPlaces;

        if( workTime.length == 2 & workTime[ 0 ] < workTime[ 1 ] )
            this.workTime = workTime;
        else{
            // Output messenge about ERROR
            // Input of new values
        }

        this.listOfPrivilege = listOfPrivilege;
    }

    //get-s
    public String    getAdress() {
        return adress;
    }
    public int       getNumbOfPlace() {
        return numbOfPlace;
    }
    public ArrayList getListOfPlaces() {
        return listOfPlaces;
    }
    public int[]     getWorkTime() {
        return workTime;
    }
    public ArrayList getListOfPrivilege() {
        return listOfPrivilege;
    }

    //set-s
    public void setAdress( String adress ) {
        this.adress = adress;
    }
    public void setNumbOfPlace( int numbOfPlace ) {
        this.numbOfPlace = numbOfPlace;
    }
    public void setListOfPlaces ( ArrayList listOfPlaces ) {
        this.listOfPlaces = listOfPlaces;
    }
    public void setWorkTime( int[] workTime ) {
        this.workTime = workTime;
    }
    public void setListOfPrivilege( ArrayList listOfPrivilege ) {
        this.listOfPrivilege = listOfPrivilege;
    }

    public int   calcEmpty(){
        int value = 0;

        for( int i = 0; i < listOfPlaces.size(); i++ )
            if( listOfPlaces.get( i ).isEmpty() )
                value++;
        return value;
    }
    public int   calcEmpty( Car car ){
        int value = 0;

        for( int i = 0; i < listOfPlaces.size(); i++ ) {
            if( listOfPlaces.get( i ).isEmpty() ){
                for( int j = 0; j < listOfPlaces.get( i ).getListOfTransport().size(); j++ ){

                    Transport TS = (Transport) listOfPlaces.get( i ).getListOfTransport().get( j );

                    if( TS.getName().equals( car.getName() ) )
                        value++;
                }
            }
        }
        return value;
    }
    public float calcCoef(){
        float value = 0;
        for( int i = 0; i < listOfPrivilege.size(); i++ )
        {
            value += listOfPrivilege.get( i ).getCoefficient();
        }
        return value;
    }
    public float calcCost( Place place, int time) {
        float cost = 0;
        cost = ( float ) ( place.calcTime( time ) * place.getCarOnPlace().getPrice());
        cost += cost * calcCoef();
        cost -= cost * place.getCarOnPlace().calcCoef();
        return cost < 0 ? 0 : 1;
    }

    private int     findPlace( Car car ){
        for( int i = 0; i < listOfPlaces.size(); ++i )
            if( listOfPlaces.get( i ).isEmpty( car ) )
                return i;
        return -1;
    }
    private Place   fPlace( Car car ){
        for( int i = 0; i < listOfPlaces.size(); ++i )
            if( listOfPlaces.get( i ).isEmpty() & listOfPlaces.get( i ).getCarOnPlace().equals( car ) )
                return listOfPlaces.get( i );

    }
    public  float   exemptPlace( Car car, int time ){
        Place place = fPlace( car );
        float cost = calcCost( place, time );
        place.exemptPlace();
        return cost;
    }

    public boolean checkTime(int time) {
        return time >= workTime[ 0 ] & time < workTime[ 1 ];
    }
    public  boolean takePlace( Car car, int btime ){
        if( calcEmpty( car ) == 0 )
        {
            System.out.println( "All places is taken" );
            return false;
        }
        int numb = findPlace( car );

        if( checkTime( btime ) & listOfPlaces.get( numb ).takePlace( car, btime ) )
            return true;
        return false;
    }

    private int     findPlace( Place place ){
        for( int i = 0; i < listOfPlaces.size(); ++i )
            if( listOfPlaces.get( i ).equals( place ) )
                return i;
        return -1;
    }
    public  boolean addPlace ( Place place ){
        int n = findPlace( place );
        if( n == -1 ) {
	        listOfPlaces.add( place );
	        ++numbOfPlace;
	        return true;
        }
        return  false;

    }
    public  boolean delPlace ( Place place ){
    	int n = findPlace( place );
    	if( n != -1 )
	    {
	    	listOfPlaces.remove( n );
	    	--numbOfPlace;
	    	return true;
	    }
	    return  false;
    }

    private int     findPrivilege( Privilege privilege ){
    	for( int i = 0; i < listOfPrivilege.size(); ++i )
    		if( listOfPrivilege.get( i ).equals( privilege ) )
    			return i;
    	return -1;
    }
	public  boolean addPrivilege( Privilege privilege ){
    	int n = findPrivilege( privilege );
    	if( n == -1 ){
    		listOfPrivilege.add( privilege );
    		return true;
	    }
	    return false;
	}
    public  boolean delPrivilege( Privilege privilege ){
    	int n = findPrivilege( privilege );
    	if( n != -1 )
	    {
	    	listOfPrivilege.remove( n );
	    	return true;
	    }
	    return false;
    }

    //DataBase
    public Parking getParking( String adress ){
        return null;
    }
    public boolean writeParking( Parking parking )
    {
        return true;
    }

	public void getDump(){
    	System.out.println( "Adress:\t" + adress );
    	System.out.println( "Work time" + workTime[ 0 ] + "-" + workTime[ 1 ] );

    	System.out.print( "Numb of places\t" + numbOfPlace );
		if ( numbOfPlace == listOfPlaces.size() ) {
			System.out.println( "\tTRUE!" );
		} else {
			System.out.println( "\tFALSE!" );
		}
		for( int i = 0; i < listOfPlaces.size(); ++i ){
			System.out.println( "Place №" + ( i + 1 ) );
			listOfPlaces.get( i ).getDump();
		}

		System.out.print( "Numb of privilege\t" + listOfPrivilege.size() );
		for( int i = 0; i < listOfPrivilege.size(); ++i ){
			System.out.println( "Privilege №" + ( i + 1 ) );
			listOfPrivilege.get( i ).getDump();
		}

	}


}
