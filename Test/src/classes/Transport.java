package classes;


public class Transport {
    protected String name;
    protected float price;

    //constructor
    public Transport( String name, float price ) {
        this.name = name;
        this.price = price;
    }

    //get-s
    public String getName(){
        return name;
    }
    public float  getPrice(){
        return price;
    }

    //set-s
    public void setName( String name ){
        this.name = name;
    }
    public void setPrice( float price ){
        this.price = price;
    }

    public boolean equals( Transport ts ) {
        return name.equals( ts.getName() ) & price == ts.getPrice();
    }

    public void getDump(){
        System.out.println( "Name:\t" + name + "\tPrice\t" + price );
    }
}
