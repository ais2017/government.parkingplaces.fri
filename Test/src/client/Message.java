package client;

public class Message {
    String messege;
    int    code;

    public String getMessege() {
        return messege;
    }

    public int getCode() {
        return code;
    }

    public Message(String messege, int code ) {
        this.messege = messege;
        this.code = code;
    }
}
