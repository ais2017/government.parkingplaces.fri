package business;

import classes.Car;
import classes.Parking;
import client.Message;
import client.fClient;
import client.tClient;


public class getPlace{
    private Parking parking;
    private Car     car;
    private int     beginTime;
    private Message message;
    private fClient in;
    private tClient out;

    public void getPlace(){
        if( fromClient() )
            getPlaceLogic();
        toClient();
    }

    private void toClient(){
        out.request( message.getMessege() );
        if( message.getCode() == 2 ) {
            message = new Message( "Do you want to register your car or to enter data again?", 0 );
            registrationCar rega = null;
            car = rega.registrationCar();
            getPlace();
            return;
        }
        if( message.getCode() != 0 )
            getPlace();
    }

    private boolean fromClient() {

        in.request();
        in.responce();

        parking.getParking( in.getAdress() );
        if ( parking == null ) {
            message = new Message("Parking with adress: " + in.getAdress() + " not found", 1 );
            return false;
        }

        car.getCar( in.getNamberTS() );
        if (car == null) {
            message = new Message("Car with number: " + in.getNamberTS() + " not found", 2 );
            return false;
        }

        return true;
    }

    public void getPlaceLogic(){

        if ( parking.checkTime( in.getTime() ) ){
            message = new Message("Parking working time: " + parking.getWorkTime()[0] + " - " + parking.getWorkTime()[1] , 3 );
            return;
        }

        if ( parking.calcEmpty( car ) > 0 ){
            message = new Message( "There are no empty places", 4);
            return;
        }


        if ( parking.takePlace( car, in.getTime() ) ){
            message = new Message("The parking is successfully occupied", 0);
            return;
        } else {
            message = new Message("Error in case of a place occupation", 5);
            return;
        }
    }



}
