package Logic.classes;

import java.util.ArrayList;

public class Parking {
	private String               ID;
    private String               address;
	private int[]                workTime;
    private int                  numbOfPlace;
    private ArrayList<Place>     listOfPlaces;
    private ArrayList<Privilege> listOfPrivilege;

    //constructor
    public Parking(){
    	workTime = new int[ 2 ];
    	listOfPlaces = new ArrayList<Place>();
    	listOfPrivilege = new ArrayList<Privilege>();
    }

    //get-s
	public String    getID() {
		return ID;
	}

	public String    getAddress() {
        return address;
    }

    public int       getNumbOfPlace() {
        return numbOfPlace;
    }

    public ArrayList getListOfPlaces() {
        return listOfPlaces;
    }

    public int[]     getWorkTime() {
        return workTime;
    }

    public ArrayList getListOfPrivilege() {
        return listOfPrivilege;
    }

    //set-s
	public void setID( String ID ){
    	this.ID = new String( ID );
	}

	public void setAddress( String address ) {
        this.address = new String( address );
    }

    public void setNumbOfPlace( int numbOfPlace ) {
        this.numbOfPlace = numbOfPlace;
    }

    public void setListOfPlaces ( ArrayList listOfPlaces ) {
        this.listOfPlaces = listOfPlaces;
    }

    public void setWorkTime( int beginTime, int endTime ) {
    	workTime = new int[ 2 ];
        workTime[ 0 ] = beginTime;
        workTime[ 1 ] = endTime;
    }

    public void setListOfPrivilege( ArrayList listOfPrivilege ) {

        this.listOfPrivilege = listOfPrivilege;
    }

    public int   calcEmpty(){
        int value = 0;

        for( int i = 0; i < listOfPlaces.size(); i++ )
            if( listOfPlaces.get( i ).isEmpty() )
                value++;
        return value;
    }
    public boolean calcEmpty( Car car ){


        for( int i = 0; i < listOfPlaces.size(); i++ ) {
            if( listOfPlaces.get( i ).isEmpty() ){
                for( int j = 0; j < listOfPlaces.get( i ).getListOfTransport().size(); j++ ){

                    String TS = ( String ) listOfPlaces.get( i ).getListOfTransport().get( j );
                    if( TS.equals( car.getName() ) )
                        return true;
                }
            }
        }
        return false;
    }
    public float calcCoef(){
        float value = 0;
        if( listOfPlaces != null )
	        for( int i = 0; i < listOfPrivilege.size(); i++ )
	        {
	            value += listOfPrivilege.get( i ).getCoefficient();
	        }
        return value;
    }
    public float calcCost( Place place, int time) {
        float cost = 0;
        cost = ( float ) ( place.calcTime( time ) * place.getCarOnPlace().getPrice());
        cost += cost * calcCoef();
        cost -= cost * place.getCarOnPlace().calcCoef();
        return cost < 0 ? 0 : cost;
    }

    private int  findNumbPlace( Car car ){
        for( int i = 0; i < listOfPlaces.size(); ++i )
            if( listOfPlaces.get( i ).isEmpty( car ) )
                return i;
        return -1;
    }
    private Place  findPlace( Car car ){
        for( int i = 0; i < listOfPlaces.size(); ++i )
            if( !listOfPlaces.get( i ).isEmpty() & listOfPlaces.get( i ).getCarOnPlace().equals( car ) )
                return listOfPlaces.get( i );
        return null;
    }

    public  float   vacatePlace( Car car, int time ){
        Place place = findPlace( car );
        float cost = calcCost( place, time );
        place.vacatePlace();
        return cost;
    }

    public boolean checkTime( int time ) {
    	boolean flag = ( time >= workTime[ 0 ] ) && ( time < workTime[ 1 ] );
        return flag;
    }

    public  int takePlace( Car car, int btime ){

        int numb = findNumbPlace( car );

        if( listOfPlaces.get( numb ).takePlace( car, btime ) )
            return listOfPlaces.get( numb ).getIDplace();
        return -1;
    }

    private int     findPlace( Place place ){
        for( int i = 0; i < listOfPlaces.size(); ++i )
            if( listOfPlaces.get( i ).equals( place ) )
                return i;
        return -1;
    }
    public  boolean addPlace ( Place place ){
        int n = findPlace( place );
        if( n == -1 ) {
	        listOfPlaces.add( place );
	        ++numbOfPlace;
	        return true;
        }
        return  false;

    }
    public  boolean delPlace ( Place place ){
    	int n = findPlace( place );
    	if( n != -1 )
	    {
	    	listOfPlaces.remove( n );
	    	--numbOfPlace;
	    	return true;
	    }
	    return  false;
    }

    private int findPrivilege( Privilege privilege ){
    	for( int i = 0; i < listOfPrivilege.size(); ++i )
    		if( listOfPrivilege.get( i ).equals( privilege ) )
    			return i;
    	return -1;
    }
	public  boolean addPrivilege( Privilege privilege ){
    	int n = findPrivilege( privilege );
    	if( n == -1 ){
    		listOfPrivilege.add( privilege );
    		return true;
	    }
	    return false;
	}
    public  boolean delPrivilege( Privilege privilege ){
    	int n = findPrivilege( privilege );
    	if( n != -1 )
	    {
	    	listOfPrivilege.remove( n );
	    	return true;
	    }
	    return false;
    }

	public void getDump(){
		System.out.println( "ID:\t" + ID );
    	System.out.println( "Address:\t" + address );
    	System.out.println( "Work time" + workTime[ 0 ] + "-" + workTime[ 1 ] );

    	System.out.print( "Numb of places\t" + numbOfPlace );
		if ( numbOfPlace == listOfPlaces.size() ) {
			System.out.println( "\tTRUE!" );
		} else {
			System.out.println( "\tFALSE!" );
		}

		System.out.print( "Numb of privilege\t" + listOfPrivilege.size() );
		for( int i = 0; i < listOfPrivilege.size(); ++i ){
			System.out.println( "Privilege №" + ( i + 1 ) );
			listOfPrivilege.get( i ).getDump();
		}

	}


}
