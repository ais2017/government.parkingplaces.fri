package Logic.classes;

import java.util.ArrayList;

public class Place {
	private int                  IDplace;
	private String               IDparking;
    private boolean              flag;
    private ArrayList<String>    listOfTransport;
    private Car                  carOnPlace;
    private int                  beginTime;

    //constructor
    public Place() {
        carOnPlace = new Car();
        listOfTransport = new ArrayList <String>();
    }

    //get-s
    public boolean   isFlag() {
        return flag;
    }

    public ArrayList getListOfTransport() {
        return listOfTransport;
    }

    public int getIDplace(){
        return IDplace;
    }

    public String getIDparking(){
        return IDparking;
    }

public Car       getCarOnPlace() {
        return carOnPlace;
    }

    public int       getBeginTime() {
        return beginTime;
    }

    //set-s
    public void setIDplace( int ID ){
        this.IDplace = ID;
    }

    public void setIDparking( String ID ){
        this.IDparking = ID;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void setListOfTransport(ArrayList listOfTransport) {
        this.listOfTransport = listOfTransport;
    }

    public void setCarOnPlace( Car car ) {
        carOnPlace = car;
    }

    public void setBeginTime( int beginTime ) {
        this.beginTime = beginTime;
    }

    public boolean isEmpty()
    {
        return flag;
    }
    public boolean isEmpty( Car car ){
        for( int i = 0; i < listOfTransport.size(); ++ i )
            if( car.getName().equals( listOfTransport.get( i )) & isEmpty() )
                return true;
        return false;
    }
    public int     calcTime( int time )
    {
        return  time - beginTime;
    }
    public void    vacatePlace(){
        flag        = true;
        carOnPlace  = null;
        beginTime   = -3808;
    }
    public boolean takePlace( Car newCar, int beginTime ){
        if( isEmpty( newCar ) ){
            flag =false;
            carOnPlace = newCar;
            this.beginTime = beginTime;
            return true;
        }
        else
            return false;
    }

    private int     findTS( String TS){
        for( int i = 0; i < listOfTransport.size(); i++ ){
            if( listOfTransport.get( i ).equals( TS ))
                return i;
        }
        return -1;
    }
    public  boolean addTS( String newTS ){
        if( findTS( newTS ) >= 0 )
        {
            listOfTransport.add( newTS );
            return true;
        }
        return false;
    }
    public  boolean deleteTS( String delTS ){
        int numb = findTS( delTS );
        if( numb >= 0 )
        {
            listOfTransport.remove( numb );
            return true;
        }
        return false;
    }

    public boolean equals( Place place ) {
        if( flag == place.isFlag() & listOfTransport.size() == place.getListOfTransport().size() )
            for( int i = 0; i < listOfTransport.size(); ++i )
                if( !listOfTransport.get( i ).equals( place.getListOfTransport().get( i ) ) )
                    return false;
        return true;
    }

    @Override
    public String toString() {
    return "Place{" +
            "IDplace=" + IDplace +
            ", IDparking=" + IDparking +
            ", flag=" + flag +
            ", listOfTransport=" + listOfTransport +
            ", carOnPlace=" + carOnPlace +
            ", beginTime=" + beginTime +
            '}';
    }


}
