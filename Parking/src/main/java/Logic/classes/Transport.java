package Logic.classes;


public class Transport {
    protected String name;
    protected int price;

    //constructor
    public Transport() {
    }

    //get-s
    public String getName(){
        return name;
    }
    public int  getPrice(){
        return price;
    }

    //set-s
    public void setName( String name ){
        this.name = name;
    }
    public void setPrice( int price ){
        this.price = price;
    }

    public boolean equals( Transport ts ) {
        return name.equals( ts.getName() ) & price == ts.getPrice();
    }


    public void getDump(){
        System.out.println( "Name:\t" + name + "\tPrice\t" + price );
    }
}
