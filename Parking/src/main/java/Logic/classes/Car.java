package Logic.classes;

import java.util.ArrayList;
import java.util.Objects;

public class Car extends Transport {

	private String ID;
	private ArrayList < Privilege > listOfPrivilege;

	//constructor
	public Car() {
		super();
	}

	//get-s
	public String getID() {
		return ID;
	}

	public ArrayList getListOfPrivilege() {
		return listOfPrivilege;
	}

	//set-s
	public void setID( String number ) {
		this.ID = number;
	}

	public void setListOfPrivilege( ArrayList listOfPrivilege ) {
		this.listOfPrivilege = new ArrayList <Privilege >();
		this.listOfPrivilege = listOfPrivilege;
	}

	//work with Privilege
	private int findPrivilege( Privilege privilege ) {
		for( int i = 0; i < listOfPrivilege.size(); ++ i )
			if( listOfPrivilege.get( i ).equals( privilege ) )
				return i;
		return - 1;
	}

	public boolean addPrivilege( Privilege privilege ) {
		int n = findPrivilege( privilege );
		if( n == - 1 ) {
			listOfPrivilege.add( privilege );
			return true;
		}
		return false;
	}

	public boolean delPrivilege( Privilege privilege ) {
		int n = findPrivilege( privilege );
		if( n != - 1 ) {
			listOfPrivilege.remove( n );
			return true;
		}
		return false;
	}

	public float calcCoef() {
		float value = 0;
		if( listOfPrivilege != null ){
			for( int i = 0; i < listOfPrivilege.size(); ++ i )
				value += listOfPrivilege.get( i ).getCoefficient();
		}
		return value;
	}

	public boolean equals( Car car ) {
		return ID.equals( car.getID() );
	}

	@Override
	public int hashCode() {

		return Objects.hash( ID, listOfPrivilege );
	}

}

