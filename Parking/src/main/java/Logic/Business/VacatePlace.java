package Logic.Business;

import DataBase.DAO.CarDAO;
import DataBase.DAO.ParkingDAO;
import DataBase.DAO.PlaceDAO;
import Logic.classes.Car;
import Logic.classes.Parking;
import Logic.classes.Place;
import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class VacatePlace{

	private int  codeError = 0;

	public VacatePlace(){}

	public int getCodeError(){
		return codeError;
	}

	public String vacatePlace( String IDcar, @NotNull HashMap<String, Object> pageVariables ){

		if( IDcar.equals( "" ) ){
			codeError = 1;
			return "Fill all fields";
		}

		CarDAO carDAO = new CarDAO();
		Car car = carDAO.selectCar( IDcar );
		if ( car == null ) {
			codeError = 2;
			return "Vehicle with number " + IDcar + " not found";
		}

		PlaceDAO placeDAO = new PlaceDAO();
		ArrayList<Place> place = placeDAO.selectPlaces( "carId", IDcar );
		if( place == null ){
			codeError = 1;
			return "Vehicle with number " + IDcar + "it is not parked";
		}

		if( place.size() != 1 ){
			codeError = 3802;
			return "Error with BD";
		}

		ParkingDAO parkingDAO = new ParkingDAO();
		Parking parking = parkingDAO.selectParking( place.get( 0 ).getIDparking() );
		if ( parking == null ) {
			codeError = 3802;
			return "Parking with number: " + place.get( 0 ).getIDparking() + " not found";
		}

		Date data = new Date( );
		pageVariables.put( "coast", parking.vacatePlace( car, data.getHours() ) );
		for( int i = 0; i< place.size(); ++i)
			if( !place.get( i ).isEmpty()){
			place.get( 0 ).setCarOnPlace( null );
			place.get( 0 ).setFlag( true );
			place.get( 0 ).setBeginTime( -3802 );}
		if( !placeDAO.insertPlaces( place ) )
		{
			codeError = 3802;
			return "ErrorDB";
		}



		pageVariables.put( "address", parking.getAddress() );
		pageVariables.put( "parking", parking.getID() );
		pageVariables.put( "IDplace", place.get( 0 ).getIDplace() );
		return "Completed";
	}


}
