package Logic.Business;

import DataBase.DAO.CarDAO;
import DataBase.DAO.TsDAO;
import Logic.classes.Car;
import Logic.classes.Transport;
import com.sun.istack.internal.NotNull;

public class RegistrationCar{

	private int codeError = 0;

	public RegistrationCar() { }

	public String registrationCar( @NotNull String carID, @NotNull String tsID ){

		if( carID.equals( "" ) | tsID.equals( "" ) ){
			codeError = 1;
			return "Fill all fields";
		}

		CarDAO carDAO = new CarDAO();
		Car car = carDAO.selectCar( carID );
		if( car != null ){
			codeError = 1;
			return "Vehicle with number" + carID + "already I was it is registered";
		}

		TsDAO tsDAO = new TsDAO();
		Transport transport = tsDAO.selectTransport( tsID );
		if( transport == null ){
			codeError = 1;
			return tsID + " - this type of the vehicle does not exist";
		}

		car = new Car();
		car.setID( carID );
		car.setName( transport.getName() );

		if( carDAO.insertCar( car ) ){
			codeError = 0;
			return "The vehicle is successfully registered";
		}

		codeError = 3802;
		return "Error with BD";
	}

}
