package Logic.Business;

import DataBase.DAO.CarDAO;
import DataBase.DAO.ParkingDAO;
import DataBase.DAO.PlaceDAO;
import Logic.classes.Car;
import Logic.classes.Parking;
import Logic.classes.Place;
import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

public class ParkCar{

	private int  codeError = 0;

	public ParkCar( ) {
	}

	public int getCodeError() {
		return codeError;
	}

	public String parkCar( @NotNull String IDparking, @NotNull String IDcar, @NotNull String time, @NotNull HashMap<String, Object> pageVariables ){

		if( IDparking.equals( "" ) | IDcar.equals( "" ) | time.equals( "" ) ){
			codeError = 1;
			return "Fill all fields";
		}

		if( !isDigit( time ) ){
			codeError = 1;
			return "Not correct format of time";
		}

		ParkingDAO parkingDAO = new ParkingDAO();
		PlaceDAO placeDAO = new PlaceDAO();

		CarDAO carDAO = new CarDAO();
		Car car = carDAO.selectCar( IDcar );
		if ( car == null ) {
			codeError = 2;
			return "Vehicle with number " + IDcar + " not found";
		}

		ArrayList<Place> place = placeDAO.selectPlaces( "carId", IDcar );
		if( place != null )
		{
			for( int i = 0; i < place.size(); ++i)
				if( !place.get( i ).isEmpty() ){
					codeError = 1;
					return "Vehicle with number " + IDcar + " is parked on parking ( ID parking " + place.get( i ).getIDparking() + " )";
				}
		}

		Parking parking = parkingDAO.selectParking( IDparking );
		if ( parking == null ) {
			pageVariables.put( "address", "Not Found" );
			codeError = 1;
			return "Parking with number: " + IDparking + " not found";
		}

		pageVariables.put( "address", parking.getAddress() );

		int beginTime = Integer.parseInt( time );
		if ( !parking.checkTime( beginTime ) ){
			codeError = 1;
			return "Parking working time: " + parking.getWorkTime()[0] + " - " + parking.getWorkTime()[1];
		}

		if ( !parking.calcEmpty( car ) ){
			pageVariables.put( "address", parking.getAddress() );
			codeError = 1;
			return "There are no empty places";
		}

		int placeID = 0;

		if ( ( placeID = parking.takePlace( car, beginTime ) ) != -1 ){
			pageVariables.put( "IDplace", placeID );
			if ( placeDAO.insertPlaces( parking.getListOfPlaces() ) ){
				codeError = 0;
				return "The parking is successfully occupied";
			}else{
				codeError = 3802;
				return "Error with BD";
			}
		} else {
			codeError = 3;
			return "Error in case of a place occupation";
		}
	}

	private static boolean isDigit(String s) throws NumberFormatException {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
