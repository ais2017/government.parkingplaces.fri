package Сlient.Servlets;

import Logic.Business.VacatePlace;
import Сlient.Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class VacatePlaceServlet extends HttpServlet {

private static HashMap<String, Object> pageVariables;

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {

        if( pageVariables == null )
            pageVariables = createPageVariablesMap( request );
        pageVariables.put( "car", "" );
        if( !pageVariables. containsKey( "message" ) )
            pageVariables.put( "message", "" );

        response.getWriter().println( PageGenerator.instance().getPage("Vacate/VacatePlace.html", pageVariables ) );

        response.setContentType( "text/html;charset=utf-8" );
        response.setStatus( HttpServletResponse.SC_OK );

    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {

        pageVariables = createPageVariablesMap(request);
        putVariablesMap( request, response, "car" );


        VacatePlace vacatePlace = new VacatePlace();
        String message = vacatePlace.vacatePlace( ( String ) pageVariables.get( "car" ), pageVariables );
        pageVariables.put( "message", message );


        switch( vacatePlace.getCodeError() ){
            case 0:
                response.getWriter().println( PageGenerator.instance().getPage( "Vacate/result_VacatePlace.html", pageVariables ) );
                break;

            case 1:
                doGet( request, response );
                break;

            case 2:
                response.getWriter().println( PageGenerator.instance().getPage( "error.html", pageVariables ) );

                response.getWriter().println( "<form method=\"GET\" action=\"/regCar\">\n" +
                                              "<input type=submit value=\"Registration Car\" name=\"reg\">\n" +
                                              "</form>");

	            response.getWriter().println( "<p>\n"+"<form method=\"GET\" action=\"/carParking\">\n" +
			                                  "<input type=submit value=\"Back\" name=\"back\">\n" +
			                                  "</form>\n"+"</p>");
                break;

            default: response.getWriter().println( PageGenerator.instance().getPage( "error.html", pageVariables ) );
                     break;
        }
    }

    private static void putVariablesMap( HttpServletRequest request,
                                         HttpServletResponse response,
                                         String name                  ) throws ServletException, IOException {

        String message = request.getParameter( name );

        response.setContentType("text/html;charset=utf-8");

        if (message == null || message.isEmpty()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } else {
            response.setStatus(HttpServletResponse.SC_OK);
        }
        pageVariables.put( name, message == null ? "" : message );
    }

    private static HashMap<String, Object> createPageVariablesMap( HttpServletRequest request ) {
        pageVariables = new HashMap<>();
        pageVariables.put("URL", request.getRequestURL().toString());
        return pageVariables;
    }
}
