package Сlient.Servlets;

import Logic.Business.RegistrationCar;
import Сlient.Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class RegistrationCarServlet extends HttpServlet {

private static HashMap<String, Object> pageVariables;

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {

        if( pageVariables == null )
            pageVariables = createPageVariablesMap( request );
        pageVariables.put( "car", "" );
        pageVariables.put( "ts", "" );
        if( !pageVariables. containsKey( "message" ) )
            pageVariables.put( "message", "" );

        response.getWriter().println(PageGenerator.instance().getPage("Registration/RegCar.html", pageVariables));

        response.setContentType( "text/html;charset=utf-8" );
        response.setStatus( HttpServletResponse.SC_OK );

    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {

        pageVariables = createPageVariablesMap(request);
        putVariablesMap( request, response, "car");
        putVariablesMap( request, response, "ts");

        RegistrationCar registrationCar = new RegistrationCar();
        String message = registrationCar.registrationCar( ( String ) pageVariables.get( "car" ), ( String ) pageVariables.get( "ts" ) );
        pageVariables.put( "message", message );

        response.getWriter().println( PageGenerator.instance().getPage( "Registration/result_RegCar.html", pageVariables ) );
    }

    private static void putVariablesMap( HttpServletRequest request,
                                         HttpServletResponse response,
                                         String name                  ) throws ServletException, IOException {

        String message = request.getParameter( name );

        response.setContentType("text/html;charset=utf-8");

        if (message == null || message.isEmpty()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } else {
            response.setStatus(HttpServletResponse.SC_OK);
        }
        pageVariables.put( name, message == null ? "" : message );
    }

    private static HashMap<String, Object> createPageVariablesMap( HttpServletRequest request ) {
        pageVariables = new HashMap<>();
        pageVariables.put("URL", request.getRequestURL().toString());
        return pageVariables;
    }
}
