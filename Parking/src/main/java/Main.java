
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import Сlient.Servlets.StartServlet;
import Сlient.Servlets.VacatePlaceServlet;
import Сlient.Servlets.ParkCarServlet;
import Сlient.Servlets.RegistrationCarServlet;

public class Main {
    public static void main(String[] args) throws Exception {
        StartServlet startServlet = new StartServlet();
        ParkCarServlet parkCarServlet = new ParkCarServlet();
        RegistrationCarServlet registrationCarServlet = new RegistrationCarServlet();
        VacatePlaceServlet vacatePlaceServlet = new VacatePlaceServlet();

        ServletContextHandler context = new ServletContextHandler( ServletContextHandler.SESSIONS );
        context.addServlet( new ServletHolder( startServlet ), "/start" );
        context.addServlet( new ServletHolder( parkCarServlet ), "/park" );
        context.addServlet( new ServletHolder( registrationCarServlet ), "/registration" );
        context.addServlet( new ServletHolder( vacatePlaceServlet ), "/vacate" );

        Server server = new Server(8080);
        server.setHandler(context);

        server.start();
        server.join();
    }
}
