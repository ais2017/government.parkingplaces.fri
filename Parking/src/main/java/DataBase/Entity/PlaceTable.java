package DataBase.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "place" )
public class PlaceTable {

	@Id
	@Column( name = "idplace" )
	private int placeId;

	@Column( name = "idparking")
	private String parkingId;

	@Column( name = "car" )
	private boolean flagCar;

	@Column( name = "bike" )
	private boolean flagBike;

	@Column( name = "truck" )
	private boolean flagTruck;

	@Column( name = "bus" )
	private boolean flagBus;

	@Column( name = "flag" )
	private boolean empty;

	@Column( name = "idcar" )
	private String carId;

	@Column( name = "timeBegin")
	private int timeBegin;

	public PlaceTable() {

	}

	public int getPlaceId() {
		return placeId;
	}

	public String getParkingId() {
		return parkingId;
	}
	public boolean isFlagCar() {
		return flagCar;
	}
	public boolean isFlagBike() {
		return flagBike;
	}
	public boolean isFlagTruck() {
		return flagTruck;
	}
	public boolean isFlagBus() {
		return flagBus;
	}
	public boolean isEmpty() {
		return empty;
	}
	public String getCarId() {
		return carId;
	}
	public int getTimeBegin() {
		return timeBegin;
	}



	public void setPlaceId( int placeId ) {
		this.placeId = placeId;
	}
	public void setParkingId( String parkingId ) {
		this.parkingId = parkingId;
	}
	public void setFlagCar( boolean flagCar ) {
		this.flagCar = flagCar;
	}
	public void setFlagBike( boolean flagBike ) {
		this.flagBike = flagBike;
	}
	public void setFlagTruck( boolean flagTruck ) {
		this.flagTruck = flagTruck;
	}
	public void setFlagBus( boolean flagBus ) {
		this.flagBus = flagBus;
	}
	public void setEmpty( boolean empty ) {
		this.empty = empty;
	}
	public void setCarId( String carId ) {
		this.carId = carId;
	}
	public void setTimeBegin( int timeBegin ) {
		this.timeBegin = timeBegin;
	}

	@Override
	public boolean equals( Object o ) {
		if( this == o ) return true;
		if( o == null || getClass() != o.getClass() ) return false;
		PlaceTable that = ( PlaceTable ) o;
		return placeId == that.placeId &&
				flagCar == that.flagCar &&
				flagBike == that.flagBike &&
				flagTruck == that.flagTruck &&
				flagBus == that.flagBus &&
				empty == that.empty &&
				Objects.equals( parkingId, that.parkingId ) &&
				Objects.equals( carId, that.carId );
	}

	@Override
	public int hashCode() {

		return Objects.hash( placeId, parkingId, flagCar, flagBike, flagTruck, flagBus, empty, carId );
	}

}
