package DataBase.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table( name = "TS" )
public class TsTable {
	@Id
	@Column( name = "idTS" )
	private String tsId;

	@Column( name = "price" )
	private int price;

	public TsTable(){}

	public String getTsId() {
		return tsId;
	}
	public int getPrice() {
		return price;
	}

	public void setTsId( String tsId ) {
		this.tsId = tsId;
	}
	public void setCarId( int carId ) {
		this.price = carId;
	}

	@Override
	public boolean equals( Object o ) {
		if( this == o ) return true;
		if( o == null || getClass() != o.getClass() ) return false;
		TsTable tsTable = ( TsTable ) o;
		return Objects.equals( tsId, tsTable.tsId ) &&
				Objects.equals( price, tsTable.price );
	}

	@Override
	public int hashCode() {

		return Objects.hash( tsId, price );
	}

	@Override
	public String toString() {
		return "TsTable{" +
				"tsId='" + tsId + '\'' +
				", carId='" + price + '\'' +
				'}';
	}
}
