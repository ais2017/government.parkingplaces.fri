package DataBase.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table( name = "place" )
public class PrivilegeTable {

	@Id
	@Column( name = "idprivilege" )
	private int privilegeId;

	@Column( name = "name")
	private String namePrivilege;

	@Column( name = "coefficient" )
	private float coefficient;

	@Column( name = "idmaster" )
	private String masterId;


	public PrivilegeTable() {

	}

	public int getPrivilegeId() {
		return privilegeId;
	}
	public String getNamePrivilege() {
		return namePrivilege;
	}
	public float getCoefficient() {
		return coefficient;
	}
	public String getMasterId() {
		return masterId;
	}

	public void setPrivilegeId( int privilegeId ) {
		this.privilegeId = privilegeId;
	}
	public void setNamePrivilege( String namePrivilege ) {
		this.namePrivilege = namePrivilege;
	}
	public void setCoefficient( float coefficient ) {
		this.coefficient = coefficient;
	}
	public void setMasterId( String masterId ) {
		this.masterId = masterId;
	}

	@Override
	public boolean equals( Object o ) {
		if( this == o ) return true;
		if( o == null || getClass() != o.getClass() ) return false;
		PrivilegeTable that = ( PrivilegeTable ) o;
		return privilegeId == that.privilegeId &&
				Float.compare( that.coefficient, coefficient ) == 0 &&
				Objects.equals( namePrivilege, that.namePrivilege ) &&
				Objects.equals( masterId, that.masterId );
	}

	@Override
	public int hashCode() {

		return Objects.hash( privilegeId, namePrivilege, coefficient, masterId );
	}

	@Override
	public String toString() {
		return "PrivilegeTable{" +
				"privilegeId=" + privilegeId +
				", namePrivilege='" + namePrivilege + '\'' +
				", coefficient=" + coefficient +
				", masterId='" + masterId + '\'' +
				'}';
	}
}
