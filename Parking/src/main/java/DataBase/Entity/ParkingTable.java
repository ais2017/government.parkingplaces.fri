package DataBase.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;


@Entity
@Table( name = "parking")
public class ParkingTable {

    @Id
    @Column( name = "idparking" )
    private String parkingId;

    @Column( name = "address" )
    private String address;

    @Column( name = "timeBeginning" )
    private int    timeBegin;

    @Column( name = "timeEnd" )
    private int    timeEnd;

    public ParkingTable(  ) {
    }

    public String getParkingId() {
        return parkingId;
    }
    public String getAddress() {
        return address;
    }
    public int getTimeBegin() {
        return timeBegin;
    }
    public int getTimeEnd() {
        return timeEnd;
    }

    public void setParkingId( String parkingId ) {
        this.parkingId = parkingId;
    }
    public void setAddress( String address ) {
        this.address = address;
    }
    public void setTimeBegin( int timeBegin ) {
        this.timeBegin = timeBegin;
    }
    public void setTimeEnd( int timeEnd ) {
        this.timeEnd = timeEnd;
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) return true;
        if( o == null || getClass() != o.getClass() ) return false;
        ParkingTable that = ( ParkingTable ) o;
        return timeBegin == that.timeBegin &&
                timeEnd == that.timeEnd &&
                Objects.equals( parkingId, that.parkingId ) &&
                Objects.equals( address, that.address );
    }

    @Override
    public int hashCode() {

        return Objects.hash( parkingId, address, timeBegin, timeEnd );
    }

    @Override
    public String toString() {
        return "ParkingTable{" +
                "parkingId='" + parkingId + '\'' +
                ", address='" + address + '\'' +
                ", timeBegin=" + timeBegin +
                ", timeEnd=" + timeEnd +
                '}';
    }
}
