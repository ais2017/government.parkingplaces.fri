package DataBase.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "car" )
public class CarTable {

	@Id
	@Column( name = "idcar" )
	private String carId;

	@Column( name = "idTS")
	private String tsId;

	public CarTable(){}

	public String getCarId() {
		return carId;
	}
	public String getTsId() {
		return tsId;
	}

	public void setCarId( String carId ) {
		this.carId = carId;
	}
	public void setTsId( String tsId ) {
		this.tsId = tsId;
	}

	@Override
	public boolean equals( Object o ) {
		if( this == o ) return true;
		if( o == null || getClass() != o.getClass() ) return false;
		CarTable carTable = ( CarTable ) o;
		return Objects.equals( carId, carTable.carId ) &&
				Objects.equals( tsId, carTable.tsId );
	}

	@Override
	public int hashCode() {

		return Objects.hash( carId, tsId );
	}

	@Override
	public String toString() {
		return "CarTable{" +
				"idcar='" + carId + '\'' +
				", idts='" + tsId + '\'' +
				'}';
	}
}
