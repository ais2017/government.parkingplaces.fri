package DataBase;

import DataBase.Entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class DBService{

	protected Session session;

	private static final String hibernate_show_sql = "true";
	private SessionFactory sessionFactory;

	private static final String DB_DIALECT  = "org.hibernate.dialect.MySQL5Dialect";
	private static final String DB_DRIVER   = "com.mysql.jdbc.Driver";
	private static final String DB_URL      = "jdbc:mysql://localhost:3306/mydb?useSSL=false";
	private static final String DB_USERNAME = "root";
	private static final String DB_PASSWORD = "geo";


	public DBService() {
		Configuration configuration = getMySqlConfiguration();
		sessionFactory = createSessionFactory( configuration );
	}

	private Configuration getMySqlConfiguration() {
		Configuration configuration = new Configuration();
		//Entity Logic.classes
		configuration.addAnnotatedClass( ParkingTable.class );
		configuration.addAnnotatedClass( CarTable.class );
		configuration.addAnnotatedClass( TsTable.class );
		configuration.addAnnotatedClass( PrivilegeTable.class );
		configuration.addAnnotatedClass( PlaceTable.class );


		configuration.setProperty( "hibernate.dialect",                 DB_DIALECT );
		configuration.setProperty( "hibernate.connection.driver_class", DB_DRIVER );
		configuration.setProperty( "hibernate.connection.url",          DB_URL );
		configuration.setProperty( "hibernate.connection.username",     DB_USERNAME );
		configuration.setProperty( "hibernate.connection.password",     DB_PASSWORD );
		configuration.setProperty( "hibernate.show_sql",                hibernate_show_sql );
		return configuration;
	}

//	public long addCar(String number, String typeTs) throws DBException {
//		try {
//			Session session = sessionFactory.openSession();
//			Transaction transaction = session.beginTransaction();
//			CarDAO dao = new CarDAO(session);
//			long id = dao.insertUser(name);
//			transaction.commit();
//			session.close();
//			return id;
//		} catch (HibernateException e) {
//			throw new DBException(e);
//		}
//	}

//	public void printConnectInfo() {
//		try {
//			SessionFactoryImpl sessionFactoryImpl = (SessionFactoryImpl) sessionFactory;
//			Connection connection = sessionFactoryImpl.getSessionFactoryOptions().getConnection();
//			System.out.println("DB name: " + connection.getMetaData().getDatabaseProductName());
//			System.out.println("DB version: " + connection.getMetaData().getDatabaseProductVersion());
//			System.out.println("Driver: " + connection.getMetaData().getDriverName());
//			System.out.println("Autocommit: " + connection.getAutoCommit());
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}

	private static SessionFactory createSessionFactory(Configuration configuration) {
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
		builder.applySettings(configuration.getProperties());
		ServiceRegistry serviceRegistry = builder.build();
		return configuration.buildSessionFactory(serviceRegistry);
	}

	public void connect() {
		Configuration configuration = getMySqlConfiguration();
		sessionFactory = createSessionFactory( configuration );
		session = sessionFactory.openSession();
	}

	public  void disconnect(){
		session.close();
	}
}
