package DataBase.DAO;

import DataBase.DBService;
import DataBase.Entity.TsTable;
import Logic.classes.Transport;
import org.hibernate.HibernateException;


public class TsDAO extends DBService{

	public TsDAO() {
	}

	public TsTable select( String name ) throws HibernateException{
		try{
			connect();
			return ( TsTable ) session.get( TsTable.class, name );
		}catch( Exception ex ){
			System.out.println( ex );
			return null;
		}finally {
			disconnect();
		}
	}

	public Transport selectTransport( String tsID ){
		Transport transport = new Transport();

		TsTable ts = select( tsID );
		if( ts != null ){
			transport.setName( ts.getTsId() );
			transport.setPrice( ts.getPrice() );

			return transport;
		}

		return null;
	}


//	public long getUserId(String name) throws HibernateException {
//		Criteria criteria = session.createCriteria(UsersDataSet.class);
//		return ((UsersDataSet) criteria.add(Restrictions.eq("name", name)).uniqueResult()).getId();
//	}
//
//	public long insertUser(String name) throws HibernateException {
//		return (Long) session.save(new UsersDataSet(name));
//	}
}
