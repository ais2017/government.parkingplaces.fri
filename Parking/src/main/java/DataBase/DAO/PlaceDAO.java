package DataBase.DAO;


import DataBase.DBService;
import DataBase.Entity.PlaceTable;
import Logic.classes.Place;
import com.sun.istack.internal.NotNull;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;


public class PlaceDAO extends DBService {

	public PlaceDAO() {}

	public ArrayList<PlaceTable> select( String parkingId ) throws HibernateException {
		return ( ArrayList<PlaceTable> ) session.createQuery( parkingId, PlaceTable.class ).list();
	}

	public ArrayList<PlaceTable> selectWithCriteria( String field, String value ) throws HibernateException{

		try {
			connect();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery < PlaceTable > query = builder.createQuery( PlaceTable.class );
			Root < PlaceTable > root = query.from( PlaceTable.class );
			query.select( root ).where( builder.equal( root.get( field ), value ) );
			Query < PlaceTable > q = session.createQuery( query );
			return ( ArrayList < PlaceTable > ) q.getResultList();
		}catch( Exception ex ){
			System.out.println( ex );
			return null;
		}finally {
			disconnect();
		}
	}

	private boolean saveOrUpdate( PlaceTable Place ) throws HibernateException {
		try{
			connect();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate( Place );
			transaction.commit();
			return true;
		}catch( Exception ex ){
			System.out.println( ex );
			return false;
		}finally{
			disconnect();
		}

	}

	public ArrayList<Place> selectPlaces( String field, String parkingId ){
		ArrayList<Place> places = new ArrayList<Place>( );

		ArrayList<PlaceTable> placesT = selectWithCriteria( field, parkingId );
		if( placesT != null )
			for( int i = 0; i < placesT.size(); ++i ){

				Place place = new Place();

				place.setIDplace( placesT.get( i ).getPlaceId() );
				place.setIDparking( placesT.get( i ).getParkingId() );

				place.setFlag( placesT.get( i ).isEmpty() );
				if( !place.isEmpty() ){
					//timeBegin
					place.setBeginTime( placesT.get( i ).getTimeBegin() );
					//carOnPlace
					CarDAO carDAO = new CarDAO();
					place.setCarOnPlace( carDAO.selectCar( placesT.get( i ).getCarId() ) );
				}else {
					place.setBeginTime( 0 );
					place.setCarOnPlace( null );
				}

				//listOfTransport
				if( placesT.get( i ).isFlagBike() )
					place.getListOfTransport().add( "bike" );
				if( placesT.get( i ).isFlagCar() )
					place.getListOfTransport().add( "car" );
				if( placesT.get( i ).isFlagBus() )
					place.getListOfTransport().add( "bus" );
				if( placesT.get( i ).isFlagTruck() )
					place.getListOfTransport().add( "truck" );

				places.add( place );
			}
		return placesT != null ? places : null;
	}

	public boolean insertPlaces( @NotNull ArrayList<Place> Places ){

		for( int i = 0; i < Places.size(); ++i ){
			PlaceTable Place = new PlaceTable();
			Place.setPlaceId( Places.get( i ).getIDplace() );
			Place.setParkingId( Places.get( i ).getIDparking() );

			for( int j = 0; j < Places.get( i ).getListOfTransport().size(); ++j ){
				if( Places.get( i ).getListOfTransport().get( j ).equals( "car" ) ){
					Place.setFlagCar( true );
				}
				if( Places.get( i ).getListOfTransport().get( j ).equals( "bus" ) ){
					Place.setFlagBus( true );
				}
				if( Places.get( i ).getListOfTransport().get( j ).equals( "truck" ) ){
					Place.setFlagTruck( true );
				}
				if( Places.get( i ).getListOfTransport().get( j ).equals( "bike" ) ){
					Place.setFlagBike( true );
				}
			}

			Place.setEmpty( Places.get( i ).isEmpty() );

			if( Places.get( i ).getCarOnPlace() != null)
				Place.setCarId( Places.get( i ).getCarOnPlace().getID() );
			else
				Place.setCarId( "NULL" );

			Place.setTimeBegin( Places.get( i ).getBeginTime() );

			if( !saveOrUpdate( Place ) )
				return false;

		}
		return true;
	}

}
