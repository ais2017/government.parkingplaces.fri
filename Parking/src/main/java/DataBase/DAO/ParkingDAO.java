package DataBase.DAO;

import DataBase.DBService;
import DataBase.Entity.ParkingTable;
import Logic.classes.Parking;
import Logic.classes.Place;
import Logic.classes.Privilege;
import org.hibernate.HibernateException;

import java.util.ArrayList;


public class ParkingDAO extends DBService{

	public ParkingDAO( ) {

	}

	public ParkingTable select( String number) throws HibernateException {
		try{
			connect();
			return ( ParkingTable ) session.get( ParkingTable.class, number );
		}finally{
			disconnect();
		}
	}

	public Parking selectParking( String parkingID ){
		Parking parking = new Parking();

		ParkingTable parkingT = select( parkingID );
		if( parkingT == null )
			return null;

		parking.setID( parkingT.getParkingId() );
		parking.setAddress( parkingT.getAddress() );
		parking.setWorkTime( parkingT.getTimeBegin(), parkingT.getTimeEnd() );

		PrivilegeDAO privilegeDAO = new PrivilegeDAO();
		ArrayList< Privilege > ListPr = privilegeDAO.selectPrivileges( "masterId", parkingID );
		if( ListPr != null )
			parking.setListOfPrivilege( ListPr );

		PlaceDAO placeDAO = new PlaceDAO();
		ArrayList< Place > ListPl = placeDAO.selectPlaces( "parkingId", parkingID );
		if( ListPl != null ){
			parking.setListOfPlaces( ListPl );
			parking.setNumbOfPlace( parking.getListOfPlaces().size() );
		}

		return parking;
	}
//	public long getUserId(String name) throws HibernateException {
//		Criteria criteria = session.createCriteria(UsersDataSet.class);
//		return ((UsersDataSet) criteria.add(Restrictions.eq("name", name)).uniqueResult()).getId();
//	}
//
}
