package DataBase.DAO;

import DataBase.DBService;
import DataBase.Entity.CarTable;
import DataBase.Entity.TsTable;
import Logic.classes.Car;
import Logic.classes.Privilege;
import com.sun.istack.internal.NotNull;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;

import java.util.ArrayList;


public class CarDAO extends DBService{

	public CarDAO() {
	}

	public CarTable get( String number) throws HibernateException {
		try{
			connect();
			return ( CarTable ) session.get( CarTable.class, number );
		}catch( Exception ex ){
			System.out.println( ex );
			return null;
		} finally {
			disconnect();
		}
	}

	private boolean saveOrUpdate( CarTable Place ) throws HibernateException {
		try{
			connect();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate( Place );
			transaction.commit();
			return true;
		}catch( Exception ex ){
			System.out.println( ex );
			return false;
		}finally{
			disconnect();
		}

	}

	public Car selectCar( String number ){
		Car car = new Car();

		CarTable carT = get( number );
		if( carT == null )
			return  null;

		TsDAO tsDAO = new TsDAO();
		TsTable tsT  = tsDAO.select( carT.getTsId() );
		if( tsT == null )
			return null;

		car.setID( carT.getCarId() );
		car.setName( carT.getTsId() );
		car.setPrice( tsT.getPrice() );

		PrivilegeDAO privilegeDAO = new PrivilegeDAO();
		ArrayList<Privilege> ListPr = privilegeDAO.selectPrivileges( "masterId", number );
		if( ListPr != null )
			car.setListOfPrivilege( ListPr );

		return car;
	}

	public boolean insertCar( @NotNull Car car ){
		CarTable carTable = new CarTable();

		carTable.setCarId( car.getID() );
		carTable.setTsId( car.getName() );

		return saveOrUpdate( carTable );
	}

//	public long getUserId(String name) throws HibernateException {
//		Criteria criteria = session.createCriteria(UsersDataSet.class);
//		return ((UsersDataSet) criteria.add(Restrictions.eq("name", name)).uniqueResult()).getId();
//	}
//
//	public long insertUser(String name) throws HibernateException {
//		return (Long) session.save(new UsersDataSet(name));
//	}


}
