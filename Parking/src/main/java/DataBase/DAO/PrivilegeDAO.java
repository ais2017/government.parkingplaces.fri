package DataBase.DAO;

import DataBase.DBService;
import DataBase.Entity.PrivilegeTable;
import Logic.classes.Privilege;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;


public class PrivilegeDAO extends DBService{

	public PrivilegeDAO() {

	}

	public ArrayList<PrivilegeTable> select( String parkingId ) throws HibernateException {
		return ( ArrayList<PrivilegeTable> ) session.createQuery( parkingId, PrivilegeTable.class );
	}

	public ArrayList<PrivilegeTable> selectWithCriteria( String field, String value ) throws HibernateException{
		try {
			connect();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery < PrivilegeTable > query = builder.createQuery( PrivilegeTable.class );
			Root < PrivilegeTable > root = query.from( PrivilegeTable.class );
			query.select( root ).where( builder.equal( root.get( field ), value ) );
			Query < PrivilegeTable > q = session.createQuery( query );
			return ( ArrayList < PrivilegeTable > ) q.getResultList();
		}catch( Exception ex ){
			return null;
		}finally {
			disconnect();
		}
	}

	ArrayList<Privilege> selectPrivileges( String field, String masterID ){
		Privilege privilege = new Privilege();
		ArrayList<Privilege> privileges = new ArrayList<Privilege>();

		ArrayList<PrivilegeTable > privilegeT = selectWithCriteria( field, masterID );
		if( privilegeT != null  )
			for( int i = 0; i < privilegeT.size(); ++i ){
				privilege.setName( privilegeT.get( i ).getNamePrivilege() );
				privilege.setCoefficient( privilegeT.get( i ).getCoefficient() );

				privileges.add( privilege );
			}

		return  privilegeT != null ? privileges : null;
	}

//	public long getUserId(String name) throws HibernateException {
//		Criteria criteria = session.createCriteria(UsersDataSet.class);
//		return ((UsersDataSet) criteria.add(Restrictions.eq("name", name)).uniqueResult()).getId();
//	}
//
//	public long insertUser(String name) throws HibernateException {
//		return (Long) session.save(new UsersDataSet(name));
//	}
}
